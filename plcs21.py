import serial
import time
import serial.tools.list_ports
import bitarray

# Requires Python 3.5.1!

# TODO: when script is run afert working with ASCII protocol the first run returns an error. the second is fine. is reproducible
# TODO: if connection is checked raise 'No connection to device or device busy' insead of '... not
# successful
# TODO: setOverCur: compute current from mA to steps

class PLCS21:


    def __init__(self, COM_PORT):

        # DEBUG
        
        self.DEBUG = True


        # Constants

        self.COM_PORT = COM_PORT          # Communication port used for serial connection
        self.DEVICE_RESPONSE_TIME = 0.001 # Time for the device to respond (seconds)
        self.PICOLAS_BUFFER_BYTES = 12    # Length of picolas command
        self.RETRY_WAIT_TIME = 0.1        # Time to wait for a retry if device is busy
        self.RETRY_MAX_TRIALS = 4         # Maximum number of trials to wait response


        # General PicoLAS command attributes

        self.CMD_PING = 0xFE01                  # ping
        self.CMD_PING_ACK = 0xFF01              # pong
        self.CMD_IDENT = 0xFE02                 # device id
        self.CMD_IDENT_ACK = 0xFF02             # device id
        self.CMD_GETHARDVER = 0xFF06            # send back hardware version being used
        self.CMD_GETHARDVER_ACK = 0xFF06        # ack
        self.CMD_GETSOFTVER = 0xFE07            # send back software version
        self.CMD_GETSOFTVER_ACK = 0xFF07        # ack
        self.CMD_GETSERIAL = 0xFE08             # send back serial number
        self.CMD_GETSERIAL_ACK = 0xFF08         # ack
        self.CMD_GETIDTRING = 0xFE09            # send back name of device
        self.CMD_GETIDTRING_ACK = 0xFF09        # ack
        self.CMD_GETDEVICECHECKSUM = 0xFE0A     # getdevice crc16 checksum of memory
        self.CMD_GETDEVICECHECKSUM_ACK = 0xFF0A # ack
        self.CMD_RESET = 0xFE0E                 # instruct to reset software. results in switch-on state
        self.CMD_RESET_ACK = 0xFF0E             # ack
        self.CMD_RXERROR_ACK = 0xFF10           # ack if faulty checksum recognised
        self.CMD_REPEAT_ACK = 0xFF11            # ack if recipient asks to send most recent frame again
        self.CMD_ILGLPARAM_ACK = 0xFF12         # ack if command valid but not parameter
        self.CMD_UNCOM_ACK = 0xFF13             # ack if command invalid


        # PLCS21 command attributes

        self.CMD_GETCPUTEMP = 0x0001           # get cpu temp
        self.CMD_GETCPUTEMP_ACK = 0x0050       # ack
        self.CMD_GETDEVTEMP = 0x0002           # get device temperature
        self.CMD_GETDEVTEMP_ACK = 0x0050       # ack
        self.CMD_GETVOLMIN = 0x0003            # get minimum voltage 0..4095
        self.CMD_GETVOLMIN_ACK = 0x0053        # ack
        self.CMD_GETVOLMAX = 0x0004            # get maximum voltage 0..4095
        self.CMD_GETVOLMAX_ACK = 0x0053        # ack
        self.CMD_GETVOLSET = 0x0005            # get current voltage specification for the pulser
        self.CMD_GETVOLSET_ACK = 0x0053        # ack
        self.CMD_GETVOLACT = 0x0006            # get actual target voltage of the pulser connected
        self.CMD_GETVOLACT_ACK = 0x0053        # ack
        self.CMD_GETVOLPERSTEP = 0x0007        # get voltage per step factor
        self.CMD_GETVOLPERSTEP_ACK = 0x0053    # ack
        self.CMD_GETCURVAL = 0x0008            # get set current in mA
        self.CMD_GETCURVAL_ACK = 0x0052        # ack
        self.CMD_GETLSTAT = 0x0009             # get laser status register
        self.CMD_GETLSTAT_ACK = 0x0054         # ack
        self.CMD_GETDEVID = 0x000A             # get device id
        self.CMD_GETDEVID_ACK = 0x0055         # ack
        self.CMD_GETPULSEWIDTH = 0x000B        # get the  pulse width in ns
        self.CMD_GETPULSEWIDTH_ACK = 0x0056    # ack
        self.CMD_GETPULSEWIDTHMIN = 0x000C     # get minimum settable pulse width
        self.CMD_GETPULSEWIDTHMIN_ACK = 0x0056 # ack
        self.CMD_GETPULSEWIDTHMAX = 0x000D     # get maximum settable pulse width
        self.CMD_GETPULSEWIDTHMAX_ACK = 0x0056 # ack
        self.CMD_GETREPRATE = 0x000E           # get actual pulse rate
        self.CMD_GETREPRATE_ACK = 0x0057       # ack
        self.CMD_GETREPRATEMIN = 0x000F        # get minimum settable pulse rate
        self.CMD_GETREPRATEMIN_ACK = 0x0057    # ack
        self.CMD_GETREPRATEMAX = 0x0010        # get minimum settable pulse rate
        self.CMD_GETREPRATEMAX_ACK = 0x0057    # ack
        self.CMD_GETSHOTS = 0x0011             # get number of shots
        self.CMD_GETSHOTS_ACK = 0x0058         # ack
        self.CMD_GETSHOTSMIN = 0x0012          # get number of minmum settable shots
        self.CMD_GETSHOTSMIN_ACK = 0x0058      # ack
        self.CMD_GETSHOTSMAX = 0x0013          # get number of maximum settable shots
        self.CMD_GETSHOTSMAX_ACK = 0x0058      # ack
        self.CMD_GETOVERCUR = 0x0014           # get value for overcurrent detection 0..4095
        self.CMD_GETOVERCUR_ACK = 0x0052       # ack
        self.CMD_GETOVERCURMIN = 0x0015        # get minimum settable overcurrent value
        self.CMD_GETOVERCURMIN_ACK = 0x0052    # ack
        self.CMD_GETOVERCURMAX = 0x0016        # get maximum settable overcurrent value
        self.CMD_GETOVERCURMAX_ACK = 0x0052    # ack
        self.CMD_GETOVERCURVAL = 0x0017        # get present overcurvalue setting in mA
        self.CMD_GETOVERCURVAL_ACK = 0x0052    # ack
        self.CMD_GETDEVTEMPOFF = 0x001B        # get switch-off-temperature
        self.CMD_GETDEVTEMPOFF_ACK = 0x0050    # ack
        self.CMD_GETDEVTEMPOFFMIN = 0x001C     # get minimumswitch-off-temperature
        self.CMD_GETDEVTEMPOFFMIN_ACK = 0x0050 # ack
        self.CMD_GETDEVTEMPOFFMAX = 0x001B     # get maximum switch-off-temperature
        self.CMD_GETDEVTEMPOFFMAX_ACK = 0x0050 # ack
        self.CMD_GETUMIN = 0x001E              # get minimum voltage
        self.CMD_GETUMIN_ACK = 0x0051          # ack
        self.CMD_GETERROR = 0x001F             # get error register
        self.CMD_GETERROR_ACK = 0x0059         # ack
        self.CMD_GETDEVICENAME = 0x0022        # get device name
        self.CMD_GETDEVICENAME_ACK = 0x005C    # ack
        self.CMD_SETVOL = 0x0030               # set voltage
        self.CMD_SETVOL_ACK = 0x0053           # ack
        self.CMD_SETLSTAT = 0x0031             # set LSTAT register
        self.CMD_SETLSTAT_ACK = 0x0054         # ack
        self.CMD_SETREPRATE = 0x0032           # set repeat rate
        self.CMD_SETREPRATE_ACK = 0x0057       # ack
        self.CMD_SETPULSEWIDTH = 0x0033        # set pulse width
        self.CMD_SETPULSEWIDTH_ACK = 0x0056    # ack
        self.CMD_SETSHOTS = 0x0034             # set number of shots
        self.CMD_SETSHOTS_ACK = 0x0058         # ack
        self.CMD_SETOVERCUR = 0x0035           # set overcurrent
        self.CMD_SETOVERCUR_ACK = 0x0052       # ack
        self.CMD_SETDEVTEMPOFF = 0x0036        # set switch-off-temperture
        self.CMD_SETDEVTEMPOFF_ACK = 0x0050    # ack
        self.CMD_SETUMIN = 0x0038              # set minimum voltage
        self.CMD_SETUMIN_ACK = 0x0051          # ack used instead of 0x0053 as in PLCS Manual Rev. 14.04
        self.CMD_CLEARERROR = 0x0039           # clear error
        self.CMD_CLEARERROR_ACK = 0x005A       # ack
        self.CMD_EXECCAL = 0x003A              # execute calibration
        self.CMD_EXECCAL_ACK = 0x005B          # ack
        self.CMD_RSTDEF = 0x003C               # reset to default values
        self.CMD_RSTDEF_ACK = 0x0060           # ack


        # Establish Serial Connection 

        self.serial = serial.Serial(baudrate = 115200,\
                      port = self.COM_PORT,\
                      bytesize = serial.EIGHTBITS,\
                      parity = serial.PARITY_EVEN,\
                      stopbits = serial.STOPBITS_ONE,\
                      timeout=1)



        # Variables

        self.is_busy = False # serial connection is occupied
        self.laser_on = False # laser status TODO 0: ? / 1: ?
        self.lstat_mode = None # operating mode: 0=Normal, 1=Frequency Generator
        self.lstat_trg_mode = None # Trigger mode
        self.lstat_voltagemode = None # Switch between current and voltage mode: TODO: 0:? / 1:?
        self.lstat_uncal = None # Indicates whether calibration data is available
        self.lstat_calibrating = None # Indicates that calibration is currently carried out


        # Check connection and set to picolas protocol using the ping

        pong = self._sendCMD(self.CMD_PING, 0)
        
        time.sleep(0.1)
        
        self.serial.reset_input_buffer()  # Not clear why but the input buffer needs to be emptied here to avoid send wrong command in the next call

        time.sleep(0.1)

        # According picolas manual send ping twice
        pong = self.ping() 
        pong = self.ping() 


        if pong == 0:

            print('PLCS21 connection successfull on port {:s}\n'.format(self.COM_PORT))

        else:

            raise ConnectionError('Not sucessfully connected to PLCS21\n')


        # Constants

        

        self.mV_PER_STEP = self._query(self.CMD_GETVOLPERSTEP, 0, self.CMD_GETVOLPERSTEP_ACK) # Get mV Per Step


        self.OVERCURVAL = self._query(self.CMD_GETOVERCURVAL, 0, self.CMD_GETOVERCURVAL_ACK) # Get mA Per Step

        self.OVERCUR = self._query(self.CMD_GETOVERCUR,0,self.CMD_GETOVERCUR_ACK)

        self.mA_PER_STEP = self.OVERCURVAL/self.OVERCUR


        return 





    # GENERAL PICOLAS COMMANDS

    def ping(self):
        """
        Is used o determine the presence of a connected recipient and to initialize
        the interface of the recipient for the PicoLAS protocol. Has no effect on the
        condition of the recipient. The command parameter is alwys 0, the anser parameter too
        
        Input: 
        Output: 0
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free
            
            self.is_busy = True

            par = self._query(self.CMD_PING, 0, self.CMD_PING_ACK)

            self.is_busy = False

        else:

            raise Exception('Ping not successful.')

        return par


    def ident(self):
        """
        I used to determine the device ID of an attached recipient. has no effect on the condition
        of the recipient. The parameter is always 0. The answer contains the ID.

        Input: 
        Output: ID
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free
            
            self.is_busy = True

            par =  par = self._query(self.CMD_IDENT, 0, self.CMD_IDENT_ACK)

            self.is_busy = False

        else:

            raise Exception('ident not successful.')

        return par


    # GET COMMANDS FOR PLCS21 INTERACTION

    def getCPUTemp(self):
        """
        Contains as return value the current temperature of the PLCS-21 in  degree Celcius -50 to 200

        Input:
        Output: Temperature -50 - +200
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_GETCPUTEMP, 0, self.CMD_GETCPUTEMP_ACK)

            self.is_busy = False

        else:

            raise Exception('GetCPUTemp not successful.')

        return par


    def getDevTemp(self):
        """
        Contains as a return value the current temperature in degree Celsius of the pulser connected to the PLCS-21.
        If no pulser is connected the value is 0.

        Input:
        Output: Temperature -50 - +200
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_GETDEVTEMP, 0, self.CMD_GETDEVTEMP_ACK)

            self.is_busy = False

        else:

            raise Exception('getDevTemp not successful.')

        return par


    def getVolMin(self):
        """
        Contains as return calue the minimum voltage that can be set for the connected
        pulser in mV. If no pulser is connected the values is 0.
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_GETVOLMIN, 0, self.CMD_GETVOLMIN_ACK)

            self.is_busy = False

        else:

            raise Exception('getVolMin not successful.')

        return par*self.mV_PER_STEP    # Return VoltageSteps * VoltagePerStep


    def getVolMax(self):
        """
        Contains as return calue the maximum voltage that can be set for the connected
        pulser in mV. If no pulser is connected the values is 0.
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_GETVOLMAX, 0, self.CMD_GETVOLMAX_ACK)

            self.is_busy = False

        else:

            raise Exception('getVolMax not successful.')

        return par*self.mV_PER_STEP    # Return VoltageSteps * VoltagePerStep


    def getVolSet(self):
        """
        Contains as return value the current voltage specification of the connected pulser in mV.
        If no pulser is connected then the value 0 is returned.

        Input:
        Output: Voltage in mV
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_GETVOLSET, 0, self.CMD_GETVOLSET_ACK)

            self.is_busy = False

        else:

            raise Exception('getVolSet not successful.')

        return par*self.mV_PER_STEP    # Return VoltageSteps * VoltagePerStep


    def getVolAct(self):
        """
        Contains as return value the measured value of the target voltage of the connected pulser in mV.
        If no pulser is connected then the value 0 is returned.

        Input:
        Output: Voltage in mV
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_GETVOLACT, 0, self.CMD_GETVOLACT_ACK)

            self.is_busy = False

        else:

            raise Exception('getVolAct not successful.')

        return par*self.mV_PER_STEP    # Return VoltageSteps * VoltagePerStep


    def getLStat(self):
        """
        Contains as a return value the laser status register:

        | Bit   | Name                | Read/Write | Meaning                                           |
        |-------+---------------------+------------+---------------------------------------------------|
        | 0     | L_ON                | Read       | Switch on/off the pulse output                    |
        | 1     | MODE                | Read       | Operation Mode: 0: normal, 1: Frequency Generator |
        | 2-5   | TRG_MODE            | Read/Write | Refer to trigger modes                            |
        | 6     | ENABLE_HELPPULSE    | Read/Write | Reserved                                          |
        | 7     | ENABLE_FEEDBACK_MON | Read/Write | Reserved                                          |
        | 8     | VOLTAGEMODE         | Read/Write | Switches between Voltage and Current Mode.        |
        | 9     | UNCAL               | Read/Write | Indicats whether calibration data is available    |
        | 10    | CALIBRATING         | Read       | Indicates that device is calibrating              |
        | 11    | Reserved            | Read       | Reserved                                          |
        | 12    | BUSY                | Read       | PLCS is not accepting commands                    |
        | 13    | INIT_COMPLETE       | Read       | Indicates successful initialization               |
        | 14    | DEVICE_CHANGED      | Read       | Inidcates that another device has been connected  |
        | 15-31 | Reserved            | Read       | Reserved                                          |


        LSTAT Bit table for trigger mode:

        | Mode | LSTAT bits |       |      |      |      | Description                          |
        |      | Bit 5      | Bit 4 | Bit3 | Bit2 | Bit1 |                                      |
        |------+------------+-------+------+------+------+--------------------------------------|
        | 0    | 0          | 0     | 0    | 0    | 0    | Ext. Trig. Falling Edge. Set N Pulse |
        | 1    | 0          | 0     | 0    | 0    | 1    | Ext. Trig. Rising Edge. Set N Pulse  |
        | 2    | 0          | 0     | 0    | 1    | 0    | Int. Trig. Ongoing pulse             |
        | 3    | 0          | 0     | 1    | 0    | 0    | Int. Trig. Ongoing pulse             |
        | 4    | 0          | 1     | 0    | 0    | 0    | Ext. Trig. Pulsing during LOW level  |
        | 5    | 1          | 0     | 0    | 0    | 0    | Ext. Trig. Pulsing during HIGH level |

        Input:
        Output: 32 bit array
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_GETLSTAT, 0, self.CMD_GETLSTAT_ACK)

            self.is_busy = False

        else:

            raise Exception('getLStat not successful.')


        return par


    def getDevID(self):
        """
        Contains as return value the id of the connected pulser.

        Input:
        Output: ID 0-32
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_GETDEVID, 0, self.CMD_GETDEVID_ACK)

            self.is_busy = False

        else:

            raise Exception('getDevID not successful.')

        return par


    def getPulseWidth(self):
        """
        Contains as return value the currently set pulse width in ns

        Input:
        Output: pulse width in ns
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_GETPULSEWIDTH, 0, self.CMD_GETPULSEWIDTH_ACK)

            self.is_busy = False

        else:

            raise Exception('getPulseWidth not successful.')

        return par


    def getPulseWidthMin(self):
        """
        Contains as return value the minimum pulse width in ns

        Input:
        Output: pulse width in ns
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_GETPULSEWIDTHMIN, 0, self.CMD_GETPULSEWIDTHMIN_ACK)

            self.is_busy = False

        else:

            raise Exception('getPulseWidthMin not successful.')

        return par


    def getPulseWidthMax(self):
        """
        Contains as return value the maximum pulse width in ns

        Input:
        Output: pulse width in ns
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_GETPULSEWIDTHMAX, 0, self.CMD_GETPULSEWIDTHMAX_ACK)

            self.is_busy = False

        else:

            raise Exception('getPulseWidthMax not successful.')

        return par


    def getRepRate(self):
        """
        Contains as return value the the currently set repeat rate in Hz

        Input:
        Output: repeat rate in Hz
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_GETREPRATE, 0, self.CMD_GETREPRATE_ACK)

            self.is_busy = False

        else:

            raise Exception('getPulseReprate not successful.')

        return par


    def getRepRateMin(self):
        """
        Contains as return value the the minimu settable repeat rate in Hz

        Input:
        Output: repeat rate in Hz
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_GETREPRATEMIN, 0, self.CMD_GETREPRATEMIN_ACK)

            self.is_busy = False

        else:

            raise Exception('getPulseRepRateMin not successful.')

        return par


    def getRepRateMax(self):
        """
        Contains as return value the the maximum settable repeat rate in Hz

        Input:
        Output: repeat rate in Hz
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_GETREPRATEMAX, 0, self.CMD_GETREPRATEMAX_ACK)

            self.is_busy = False

        else:

            raise Exception('getPulseRepRateMax not successful.')

        return par


    def getOverCur(self):
        """
        Contains as return value the presently set value for over-current detection. This is
        indicated in the range from 0...4095. If a value in mA is requred, then this can be queried
        with GETOVERCURVAL.
        This register is only used if the PLCS-21 i not being used as frequency generator.
        mode.

        Input:
        Output: overcurrent
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_GETOVERCUR, 0, self.CMD_GETOVERCUR_ACK)

            self.is_busy = False

        else:

            raise Exception('getOverCurVal not successful.')

        return par


    def getOverCurMin(self):
        """
        Contains as return value the minimum settable value for over-current detection. This is
        indicated in the range from 0...4095. If a value in mA is requred, then this can be queried
        with GETOVERCURVAL.
        This register is only used if the PLCS-21 i not being used as frequency generator.
        mode.

        Input:
        Output: overcurrentmin
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_GETOVERCURMIN, 0, self.CMD_GETOVERCURMIN_ACK)

            self.is_busy = False

        else:

            raise Exception('getOverCurValMin not successful.')

        return par



    def getOverCurMax(self):
        """
        Contains as return value the maximum settable value for over-current detection. This is
        indicated in the range from 0...4095. If a value in mA is requred, then this can be queried
        with GETOVERCURVAL.
        This register is only used if the PLCS-21 i not being used as frequency generator.
        mode.

        Input:
        Output: overcurrentmin
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_GETOVERCURMAX, 0, self.CMD_GETOVERCURMAX_ACK)

            self.is_busy = False

        else:

            raise Exception('getOverCurValMax not successful.')

        return par


    def getOverCurVal(self):
        """
        Contains as return value the presently set value for over-current detection in mA.
        This register is only used if the PLCS-21 i not being used as frequency generator.
        mode.

        Input:
        Output: overcurrent value
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_GETOVERCURVAL, 0, self.CMD_GETOVERCURVAL_ACK)

            self.is_busy = False

        else:

            raise Exception('getOverCurVal not successful.')

        return par



    def getDevTempOff(self):
        """
        Contains as return value the presently set switch-off temperature in deg Celsius for the
        pulser which is connected. When this temperature is reached the pulser emission is stopped
        and a temperature error is raised.
        This register is only used if the PLCS-21 i not being used as frequency generator.
        mode.

        Input:
        Output: device switch-off tmperature in degree Celcius
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_GETDEVTEMPOFF, 0, self.CMD_GETDEVTEMPOFF_ACK)

            self.is_busy = False

        else:

            raise Exception('getDevTempOff not successful.')

        return par

    def getDevTempOffMin(self):
        """
        Contains as return value the minimum settable switch-off temperature in deg Celsius for the
        pulser which is connected. This register is only used if the PLCS-21 i not being used as frequency generator.
        mode.

        Input:
        Output: minimum switch-off temperature in degree Celcius
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_GETDEVTEMPOFFMIN, 0, self.CMD_GETDEVTEMPOFFMIN_ACK)

            self.is_busy = False

        else:

            raise Exception('getDevTempOffMin not successful.')

        return par


    def getDevTempOffMax(self):
        """
        Contains as return value the maximum settable switch-off temperature in deg Celsius for the
        pulser which is connected. This register is only used if the PLCS-21 i not being used as frequency generator.
        mode.

        Input:
        Output: maximum switch-off temperature in degree Celcius
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_GETDEVTEMPOFFMAX, 0, self.CMD_GETDEVTEMPOFFMAX_ACK)

            self.is_busy = False

        else:

            raise Exception('getDevTempOffMax not successful.')

        return par


    def getUMin(self):
        """
        Contains as return value the presently set start voltage for the calibration in mV. 
        This register is only used if the PLCS-21 i not being used as frequency generator.
        mode.

        Input:
        Output: currently set minimum voltage used to start calibration
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_GETUMIN, 0, self.CMD_GETUMIN_ACK)

            self.is_busy = False

        else:

            raise Exception('getUMin not successful.')

        return par*self.mV_PER_STEP


    def getError(self):
        """
        Contains as return value the present content of the error register.

        Input:
        Output: error register
        """

        # TODO how the error register works with 32 bytes?
        if self._checkSerialConnection() & self._checkDeviceStatus():

            self._is_busy = True

            par = self._query(self.CMD_GETERROR, 0, self.CMD_GETERROR_ACK)

            self._is_busy = False

        else:

            raise Exception('getError not successful.')

        return bin(par) # Return binary representation of error register


    def getDeviceName(self):
        """
        Instructs the recipient to send back a string which contains the name of the connected
        pulser. If 0 is sent as a parameter, the answer contains the number of digits of the string,
        otherwise the respective position of the name is sent in ASCII format.

        Input:
        Output:
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_GETDEVICENAME, 1, self.CMD_GETDEVICENAME_ACK)

            self.is_busy = False

        else:

            raise Exception('getDeviceName not successful')

        return par


    # Setter commands, require a parameter

    def setVol(self, voltage_mV):
        """
        Changes the target specification of the set voltage. Only the range which is defined by
        GETVOLMIN and GETVOLMAX is allowed. Outside this range the sender will receive a ILGLPARAM
        as answer. The answer parameter is the set target value.

        Input: voltage (mV)
        Output: voltage (mV) or ILGLPARAM
        """

        voltage_step = round(voltage_mV/self.mV_PER_STEP)

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_SETVOL, voltage_step, self.CMD_SETVOL_ACK)

            self.is_busy = False

        else:

            raise Exception('setVol not successful.')

        return True


    def _setLStat(self, lstat):
        """
        Changes the laser status register to the passed parameter:

        | Bit   | Name                | Read/Write | Meaning                                           |
        |-------+---------------------+------------+---------------------------------------------------|
        | 0     | L_ON                | Read       | Switch on/off the pulse output                    |
        | 1     | MODE                | Read       | Operation Mode: 0: normal, 1: Frequency Generator |
        | 2-5   | TRG_MODE            | Read/Write | Refer to trigger modes                            |
        | 6     | ENABLE_HELPPULSE    | Read/Write | Reserved                                          |
        | 7     | ENABLE_FEEDBACK_MON | Read/Write | Reserved                                          |
        | 8     | VOLTAGEMODE         | Read/Write | Switches between Voltage and Current Mode.        |
        | 9     | UNCAL               | Read/Write | Indicats whether calibration data is available    |
        | 10    | CALIBRATING         | Read       | Indicates that device is calibrating              |
        | 11    | Reserved            | Read       | Reserved                                          |
        | 12    | BUSY                | Read       | PLCS is not accepting commands                    |
        | 13    | INIT_COMPLETE       | Read       | Indicates successful initialization               |
        | 14    | DEVICE_CHANGED      | Read       | Inidcates that another device has been connected  |
        | 15-31 | Reserved            | Read       | Reserved                                          |


        LSTAT Bit table for trigger mode:

        | Mode | LSTAT bits |       |      |      |      | Description                          |
        |      | Bit 5      | Bit 4 | Bit3 | Bit2 | Bit1 |                                      |
        |------+------------+-------+------+------+------+--------------------------------------|
        | 0    | 0          | 0     | 0    | 0    | 0    | Ext. Trig. Falling Edge. Set N Pulse |
        | 1    | 0          | 0     | 0    | 0    | 1    | Ext. Trig. Rising Edge. Set N Pulse  |
        | 2    | 0          | 0     | 0    | 1    | 0    | Int. Trig. Ongoing pulse             |
        | 3    | 0          | 0     | 1    | 0    | 0    | Int. Trig. Ongoing pulse             |
        | 4    | 0          | 1     | 0    | 0    | 0    | Ext. Trig. Pulsing during LOW level  |
        | 5    | 1          | 0     | 0    | 0    | 0    | Ext. Trig. Pulsing during HIGH level |

        Input: 32 bit array

        Output:
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_SETLSTAT, lstat, self.CMD_SETLSTAT_ACK)

            self.is_busy = False

        else:

            raise Exception('setLStat not successful.')

        return par


    def setRepRate(self, rep_rate_Hz):
        """
        Changes the target specification of the pulses to the passed value. Only the range which is
        defined by GETREPRATEMIN and GETREPRATEMAX is permissible. Outside of this range the sender
        will receive a ILGLPARAM as answer. _query will raise an error if the wrong parameters are
        passed. The answer parameter is the set repeat rate.

        Input: repetiton rate
        Output:
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # Send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_SETREPRATE, rep_rate_Hz, self.CMD_SETREPRATE_ACK)

            self.is_busy = False

            self.rep_rate = rep_rate_Hz

        else:

            raise Exception('setRepRate not successful.')

        return True


    def setPulseWidth(self, pulse_width_ns):
        """
        Changes the width of the pulse to the passsed value. Only the range which is defined by
        GETPULSEWITHMIN and GETPULSEWIDTHMAX is permissible. Outside of this range the sender will
        receive a ILGLPARAM as answer. _query will raise an error if the wrong parameters are
        passed. 

        Input: pulse width in nano seconds
        Output:
        """

        if self._checkSerialConnection() & self._checkDeviceStatus():

            self.is_busy = True

            par = self._query(self.CMD_SETPULSEWIDTH, pulse_width_ns, self.CMD_SETPULSEWIDTH_ACK)

            self.is_busy = False

        else:

            raise Exception('setRepRate not successful.')

        return True



    def setOverCur(self,over_current_mA):
        
        """
        Changes the maximum permissible diode current to the passed value. Only the range which is
        defined by GETOVERCURMIN and GETOVERCURMAX is permissible. _query will raise and error if
        the wrong parameters are passed.

        Input: over current
        Output: 
        """

        if self._checkSerialConnection() & self._checkDeviceStatus():

            self.is_busy = True

            par = self._query(self.CMD_SETOVERCUR, round(over_current_mA/self.mA_PER_STEP), self.CMD_SETOVERCUR_ACK)

            self.is_busy = False

        else:

            raise Exception('setOverCur not successful')

        return True

## This Command returns 0xff13 saying illigal command!???
##
##    def setDevTempOff(self, dev_temp_off):
##        """
##        Changes the switch-off temperature to the passed value. Only the range which is defined by
##        GETDEVTEMPMIN and GTDEVTEMPMAX is permissible. _query will raise an error if the wrong
##        paramers are passed. 
##
##        Input: pulse width
##        Output:
##        """
##
##        if self._checkSerialConnection() & self._checkDeviceStatus():
##
##            self.is_busy = True
##
##            par = self._query(self.CMD_SETDEVTEMPOFF, dev_temp_off, self.CMD_SETDEVTEMPOFF_ACK)
##
##            self.is_busy = False
##
##        else:
##
##            raise Exception('setDevTempOff not successful.')
##
##        return True


    def setUMin(self, u_min_mV):
        """
        Changes the minimum voltage for the calibration to the passed value. ONly the range which is
        sdefined by GETVOLMIN and GETVOLMAX is permissible. _query will raise an error if the wrong
        paramers are passed. 

        Input: Minimum Voltage
        Output:
        """

        if self._checkSerialConnection() & self._checkDeviceStatus():

            self.is_busy = True

            par = self._query(self.CMD_SETUMIN, round(u_min_mV/self.mV_PER_STEP), self.CMD_SETUMIN_ACK)

            self.is_busy = False

        else:

            raise Exception('setUMin not successful.')

        return True


    def clearError(self):
        """
        Delets the ERROR register and set the PLCS21 to an error-free condition

        Input:
        Output:
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_CLEARERROR, 0, self.CMD_CLEARERROR_ACK)

            self.is_busy = False

        else:

            raise Exception('clearError not successful.')

        return True


    def execCal(self):
        """
        Instruct the pulser to carry out a calibration. If the _query answer is 0 the calibration is
        initiated. If it is not equal zero it is currntly not possible to carry out a calibration.
        This occurs if another calibation is currently carried out or no pulser is connected.

        Input:
        Output: bool
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_EXECCAL, 0, self.CMD_EXECCAL_ACK)

            self.is_busy = False

        else:

            raise Exception('execCal not successful.')

        if par == 0:

            return True

        else:

            return False


    def rstDef(self):
        """
        Instructs the pLCS-21 to reset all parameters to factory defaults. All valiration data will
        be lost. The default values are the minimum values for the PLCS-21 or the connected laser
        diode driver.

        Input:
        Output:
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # send query if device is free

            self.is_busy = True

            par = self._query(self.CMD_RSTDEF, 0, self.CMD_RSTDEF_ACK)

            self.is_busy = False

        else:

            raise Exception('rstDef not successful.')

        return True





    def getLaserState(self):
        """
        Get the laser state: TODO: 0: ?, 1: ?

        Input: 
        Output: state
        """
        if self._checkSerialConnection() & self._checkDeviceStatus(): # send query if device is free

            self.is_busy = True

            lstat = self._query(self.CMD_GETLSTAT, 0, self.CMD_GETLSTAT_ACK)

            self.is_busy = False

        return lstat & 0b1 # Test this


    def setLaserOn(self):
        """
        Switch the laser on
        """
        if self._checkSerialConnection() & self._checkDeviceStatus(): # send query if device is free

            self.is_busy = True

            lstat = self._query(self.CMD_GETLSTAT, 0, self.CMD_GETLSTAT_ACK)

            lstat_send = lstat | 0b1    # Switch laser on

            self._query(self.CMD_SETLSTAT, lstat_send, self.CMD_SETLSTAT_ACK) 

            self.is_busy = False

            self.laser_on = True

            return True


    def setLaserOff(self):
        """
        Switch the laser off
        """

        if self._checkSerialConnection() & self._checkDeviceStatus(): # send query if device is free

            self.is_busy = True

            lstat = self._query(self.CMD_GETLSTAT, 0, self.CMD_GETLSTAT_ACK)

            lstat_send = lstat & ~0b1   # Switch laser off

            self._query(self.CMD_SETLSTAT, lstat_send, self.CMD_SETLSTAT_ACK) 

            self.is_busy = False

        self.laser_on = False

        return True


    
    # PicoLAS protocol Methods

    def toBytes(self, cmd, par):
        """
        Convert command and parameter to PicoLAS protocol
        Input: Command, Parameter
        """
        buff = bytearray(12)

        buff[0] = cmd & 0xFF
        buff[1] = ( cmd >> 8 ) & 0xFF
        buff[2] = par & 0xFF
        buff[3] = ( par >> 8 ) & 0xFF
        buff[4] = ( par >> 16 ) & 0xFF
        buff[5] = ( par >> 24 ) & 0xFF
        buff[6] = ( par >> 32 ) & 0xFF
        buff[7] = ( par >> 40 ) & 0xFF
        buff[8] = ( par >> 48 ) & 0xFF
        buff[9] = ( par >> 56 ) & 0xFF
        buff[10] = 0
        buff[11] = self.checksum(buff)

        return buff


    def fromBytes(self, buff):
        """
        Convert from PicoLAS Byte Array to Command and Parameter
        """

        cmd = buff[0]
        cmd += buff[1] << 8
        par = buff[2]
        par += buff[3] << 8
        par += buff[4] << 16
        par += buff[5] << 24
        par += buff[6] << 32
        par += buff[7] << 40
        par += buff[8] << 48
        par += buff[9] << 56

        return cmd, par


    def checksum(self,buff):
        """
        Compute checksum XOR from binary array
        Input type: bytesarray
        """

        out = 0
        for i in range(len(buff)-1):
            out ^= buff[i]
        return out


    def _query(self, cmd, par, ack_expected):
        """
        Sends a query to the PLCS21 and waits for an answer. Returns the parameter if the command from the received frame
        matches the expected acknowledgement.
        Can be used to set values for a specific command or to get values for a specific command.

        Example: get the actual cpu temperature

           >> self._query(0x0001, 0, 0x0050)
           59

        Input: Command, parameter, expected acknowledgement
        Output: Value
        """

        trials = 0 # No of attempts to send and receive a valid query


        self.serial.write(self.toBytes(cmd, par)) # Write query
        

        time.sleep(self.DEVICE_RESPONSE_TIME) # Wait for device response


        while True: # Try to receive and answer from the device

            buff = self.serial.read(self.PICOLAS_BUFFER_BYTES) # Read 12 bytes from Serial Port
            
            ack, par = self.fromBytes(buff) # Unpack Acknowledgement and Parameter
            
            if ack == ack_expected: # Everything goes well

                break

            elif trials >=self.RETRY_MAX_TRIALS:

                raise Exception('Stopped query after {:d} trials were not successful.'.format(trials))

            elif ack == self.CMD_ILGLPARAM_ACK: # Recipient received illigal parameter

                raise ValueError('"{:d}" is not a valid parameter for 0x{:04X}'.format(par,cmd))

            elif ack == self.CMD_UNCOM_ACK: # Recipient received not a valid command

                raise NameError('0x{:04X} is not a valid command.'.format(cmd))

            elif ack == self.CMD_RXERROR_ACK: #Recipient received a faulty checksum

                raise Exception('Faulty checksum: check your connection')

            elif ack == self.CMD_REPEAT_ACK: # Recipient asks to send last frame again

                self.serial.write(self.toBytes(cmd, 0)) # Write query

                time.sleep(self.DEVICE_RESPONSE_TIME) # Wait for device response

            else:

                print('Expected command acknowledement (0x{:04X}) does not match recipients acknowledement (0x{:04X})\n Most probably wrong expected acknowledement set in _query().'.format(ack_expected, ack))

            trials +=1

        return par


    def _sendCMD(self, cmd, par):
        """
        Sends a command to the PLCS21 and without waiting for an answer. This function is used only the first time to initialize the PicoLAS protocol.
        Can be used to set values for a specific command or to get values for a specific command.

        Example: send a command

           >> self._sendCMD(0x0001, 0)
           

        Input: Command, parameter
        Output: 
        """

        self.serial.reset_input_buffer() # Clear the input buffer

        self.serial.reset_output_buffer() # Clear the output buffer

        self.serial.write(self.toBytes(cmd, 0)) # Send command

        time.sleep(self.DEVICE_RESPONSE_TIME) # Wait for the device some time  

        return 


    def _checkSerialConnection(self):
        """ Check if serial connection is open """

        if not self.serial.isOpen():    # Check if serial connection is open

            raise ConnectionError('No open serial connection.')

        else:

            return True


    def _checkDeviceStatus(self):
        """ Check if the device is busy """

        n_trial = 0

        if self.is_busy:    # Check if the device is busy

            while self.is_busy:

                time.sleep(self.RETRY_WAIT_TIME)    # Wait

                n_trial += 1

                if n_trial >= self.RETRY_MAX_TRIALS:    # Raise Error after too many trials

                    raise TimeoutError('Serial connection is busy after {:d} attempts.'.format(self.RETRY_MAX_TRIALS))

        else:

            return True

## EXECUTE IF FILE IS RUN FROM CONSOLE

if __name__ == '__main__':


    def findPort(serialnumber):
        """
        Find the serial usb port
        TODO: Discrimination between multiple serial USB ports
        connection function:
        - find and list all ports
        - try to connect and get serial number/ device id for all ports found
        - Return port with device attached matching the serial number
        - activate picolas protocol ping ping
        """
        ports = list(serial.tools.list_ports.comports())
        for p in ports:
            print('Found {:s}'.format(p.description))
            #Make sure this is a unique description of the port!
            if 'USB Serial Port' in p.description:
                port = p.device
                print('Use {:s}'.format(port))

        return port



    plcs = PLCS21('COM4')

    

